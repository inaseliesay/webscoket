const express = require('express');
var socket = require('socket.io');
const app = express();
const server = app.listen(3000);
// Static files
app.use(express.static('public'));
// Socket Setup
var io = socket(server);
io.on('connection',function(socket){
	console.log('Socket Connection', socket.id);
	socket.on('chat', function(data){
		io.sockets.emit('chat',data);
	});
	
	// Handle typing event
    socket.on('typing', function(data){
        socket.broadcast.emit('typing', data);
    });
});